import { Resolver, Mutation, Args, Query } from '@nestjs/graphql';
import { StudentType } from './student.type';
import { StudentService } from './student.service';
import { CreateStudentInput } from './create-student.input';

@Resolver(of => StudentType)
export class StudentResolver {
    constructor(
        private studentService: StudentService
    ) {}

    @Mutation(returns => StudentType)
    createStudent(
        @Args('createStudentInput') createStudentInput: CreateStudentInput
    ) {
        return this.studentService.createStudent(createStudentInput)
    }

    @Query(returns => StudentType)
    getStudentById(
        @Args('id') id: string
    ) {
        return this.studentService.getStudentByID(id)
    }

    @Query(returns => [StudentType])
    getAllStudents() {
        return this.studentService.getAllStudents()
    }
}
